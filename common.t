local List = require 'terralist'

local common = {}

local function underlying_f_type(F)
  if F:ispointer() then F = F.type end

  -- terra function
  if not F:isstruct() then return F end

  local apply = F.metamethods.__apply

  -- typed lambda
  if terralib.isfunction(apply) then
    local t = apply.type
    t.lambda = true

    return t
  end

  -- generic lambda has no underlying function type
  return nil
end

function common.function_result(F, ...)
  local ftype = underlying_f_type(F)
  if ftype then
    return ftype.returntype
  end

  -- generic lambda
  local apply = F.metamethods.__apply
  local argtypes = {...}
  local selfsym = symbol(F)
  local argsyms = {}
  for _,t in ipairs(argtypes) do table.insert(argsyms, symbol(t)) end

  return (`apply(selfsym, [argsyms])):gettype()
end

function common.paramlist(F)
  local ftype = underlying_f_type(F)

  -- terra function
  if not ftype.lambda then return ftype.parameters end

  -- lambda
  local nonself = List()
  for i = 2, #ftype.parameters do
    nonself[i-1] = ftype.parameters[i]
  end

  return nonself
end

function common.expr_type(e)
  if terralib.issymbol(e) then return e.type end
  if terralib.isquote(e) then return e:gettype() end
  if terralib.isfunction(e) then return &e.type end

  error(tostring(e) .. ' should be one of the following: symbol, quote, function')
end

-- non-quotes are either variables or functions, both of which are always lvalues
function common.is_lvalue(e)
  return not terralib.isquote(e) or e:islvalue()
end

function common.dbgprint(...)
  print(...)
  return 0
end

return common
