-- result.t: value of either valid type T or error type E
local common = require 'common'
local variant = require 'variant'

local result = {}

function result.error_t(E)
  local struct error_t{ err: E }
  error_t.name = string.format('result.error_t(%s)', E)

  return error_t
end
result.error_t = terralib.memoize(result.error_t)

function result.error(v)
  local T = common.expr_type(v)
  return `[result.error_t(T)]{[v]}
end
result.error = macro(result.error)

function result.of(T, E)
  local error_t = result.error_t(E)
  local result_t = variant.of(T, error_t)

  result_t.name = string.format('result.of(%s, %s)', T, E)
  result_t.error_t = error_t
  result_t.ok_t = T

  local error_idx = result_t:type_to_index(error_t)
  terra result_t:valid()
    return self.index ~= error_idx
  end

  terra result_t:value()
    return [variant.get(`@self, T)]:value()
  end

  terra result_t:error()
    return &[variant.get(`@self, error_t)]:value().err
  end

  terra result_t.ok(val: T): result_t return val end
  terra result_t.err(val: E): result_t return error_t{val} end

  return result_t
end
result.of = terralib.memoize(result.of)

return result
