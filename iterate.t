import 'lambda'

local c_io = terralib.includec 'stdio.h'
local c_lib = terralib.includec 'stdlib.h'
local common = require 'common'
local optional = require 'optional'

local iterate = {}

-- iterators *must* implement two methods
-- get: current value (optional.of(T))
-- shift: go to next value

-- iterators can implement any of the following methods
-- prev: go to previous value
-- after(n): shift n (should be constant-time, generates shift and prev)
-- size: exact number of elements

function iterate.is_iterator(ItT)
  return
    -- needs to be a struct
    ItT.methods and
    -- must have a get method; get must return optional
    ItT.methods.get and common.function_result(ItT.methods.get.type, &ItT).of and
    -- must have a shift method; shift must return "void"
    ItT.methods.shift and common.function_result(ItT.methods.shift.type, &ItT) == tuple()
end

function iterate.value_type(ItT) return ItT.methods.get.type.returntype.of end

local function setup_iterator(t)
  -- implementing shift and prev if after is present
  if t.methods.after then
    terra t:shift() return self:after(1) end
    terra t:prev() return self:after(-1) end
  end

  -- checking mandatory methods
  assert(t.methods.get, 'iterators must implement get')
  assert(t.methods.shift, 'iterators must implement shift')

  -- function-call shortcut for get/shift
  t.metamethods.__apply = terra(self: &t)
    var c = self:get()
    if c.valid then self:shift() end

    return c
  end

  -- method syntax for iterators
  t.metamethods.__methodmissing = macro(function(method, it, ...)
    local f = iterate[method]

    if method == 'size' then
      -- warn users about trying to get a size from sizeless iterators (e.g. iterate.count())
      assert(f, string.format('iterator %s does not have a size', t))
    else
      assert(f, string.format('error: no such method %s for iterators', method))
    end

    return f(it, ...)
  end)
end

-- iterator constructors
iterate.count = macro(function()
  local struct count_t{ n: int }
  terra count_t:after(i: int) self.n = self.n + i end
  terra count_t:get(): optional.of(int) return self.n end
  count_t.infinite = true

  setup_iterator(count_t)

  return `count_t{0}
end)

iterate.from_array_ptr = macro(function(arrptr, n)
  local ArrT = common.expr_type(arrptr)
  assert(ArrT:ispointer(), tostring(arrptr) .. " must be a pointer but isn't")
  local T = ArrT.type

  local struct from_array_ptr_t{ arrptr: &T, size: int, i: int }
  terra from_array_ptr_t:after(n: int) self.i = self.i + n end
  terra from_array_ptr_t:get(): optional.of(&T)
    if self.i < 0 or self.i >= self.size then return {} end
    return &self.arrptr[self.i]
  end
  terra from_array_ptr_t:size() return self.size end

  setup_iterator(from_array_ptr_t)

  return `from_array_ptr_t{arrptr, n, 0}
end)

iterate.from_array = macro(function(arr)
  local ArrT = common.expr_type(arr)
  assert(ArrT:isarray(), tostring(arr) .. " must be an array but isn't")
  local T = ArrT.type

  return `iterate.from_array_ptr([&T](arr), ArrT.N)
end)

-- iterator algorithms
function iterate.at(it, i)
  local ItT = common.expr_type(it)
  assert(ItT.methods.after, string.format('iterator %s is not random-access', ItT))

  local terra at_f(it_: ItT, i_: int)
    it_:after(i_)
    var v = it_:get()
    it_:after(0 - i_) -- return to original position

    return v
  end

  return `at_f(it, i)
end

function iterate.each_of(it, f)
  local ItT = common.expr_type(it)
  local F = common.expr_type(f)

  local terra each_of_f(it_: ItT, f_: F)
    var cur = it_()

    while cur.valid do
      f_(cur:value())
      cur = it_()
    end
  end

  return `each_of_f(it, f)
end

function iterate.all_of(it, cond)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)
  local Cond = common.expr_type(cond)

  local terra all_of_f(it_: ItT, cond_: Cond): bool
    repeat
      var cur = it_()
      if not cur:map([lam(t: T) cap(cond_) cond_(t)]):value_or(true) then
        return false
      end
    until not cur.valid

    return true
  end

  return `all_of_f(it, cond)
end

function iterate.any_of(it, cond)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)
  local Cond = common.expr_type(cond)

  local terra any_of_f(it_: ItT, cond_: Cond): bool
    return not it_:all_of([lam(v: T) cap(cond_) not cond_(v)])
  end

  return `any_of_f(it, cond)
end

function iterate.all_equal(it)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)

  local terra all_equal_f(it_: ItT): bool
    var first = it_()
    return first
      :map([lam(first: T) cap(it_)
        it_:all_of([lam(v: T) cap(first) v == first])])
      :value_or(true) -- all_equal should hold for empty iterators
  end

  return `all_equal_f(it)
end

-- iterator transformers
function iterate.get_n(it, n)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)

  local struct get_n_t{ i: int, n: int, src: ItT }
  terra get_n_t:get(): optional.of(T)
    if self.i >= self.n then return {} end
    return self.src:get()
  end

  if ItT.methods.after then
    terra get_n_t:after(n: int)
      self.i = self.i + n
      return self.src:after(n)
    end
  else
    if ItT.methods.prev then
      terra get_n_t:prev()
        self.i = self.i - 1
        return self.src:prev()
      end
    end
    terra get_n_t:shift()
      self.i = self.i + 1
      return self.src:shift()
    end
  end

  if ItT.methods.size then
    terra get_n_t:size()
      if self.n < self.src:size() then return self.n else return self.src:size() end
    end
  elseif ItT.infinite then
    terra get_n_t:size() return self.n end
  end

  setup_iterator(get_n_t, terra(self: &get_n_t): optional.of(T)
    if self.i >= self.n then return {} end

    var cur = self.src()
    self.i = self.i + 1

    return cur
  end)

  return `get_n_t{0, n, it}
end

function iterate.map(it, f)
  local ItT = common.expr_type(it)
  local T = common.function_result(ItT).of
  local F = common.expr_type(f)

  local struct map_t{ src: ItT, f: F }
  terra map_t:get()
    return self.src:get():map([lam(src: T) cap(self)
      self.f(src)])
  end

  if ItT.methods.after then
    terra map_t:after(n: int) self.src:after(n) end
  else
    if ItT.methods.prev then
      terra map_t:prev() self.src:prev() end
    end
    terra map_t:shift() self.src:shift() end
  end

  if ItT.methods.size then
    terra map_t:size() return self.src:size() end
  end
  map_t.infinite = ItT.infinite

  setup_iterator(map_t)

  return `map_t{it, f}
end

-- FIXME an infinite source might cause filter to hang if no values after a
-- certain position pass the filter (e.g.: inf_it:filter([lam(n) n < 123456]))
function iterate.filter(it, cond)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)
  local Cond = common.expr_type(cond)

  local struct filter_t{ src: ItT, cond: Cond }
  terra filter_t:get(): optional.of(T)
    if not self.src:get().valid then return {} end

    var val = self.src:get():value()
    if not self.cond(val) then self:shift() end

    return self.src:get()
  end

  terra filter_t:shift()
    var cur = self.src()
    while cur.valid do
      var val = cur:value()
      if self.cond(val) then break end

      cur = self.src()
    end
  end

  -- considering the situation described above is an error and freezes the
  -- program, we can assume the following is true otherwise:
  filter_t.infinite = ItT.infinite

  setup_iterator(filter_t)

  return `filter_t{it, cond}
end

function iterate.cycle(it)
  local ItT = common.expr_type(it)
  local struct cycle_t{ cur_src: ItT, orig: ItT }

  terra cycle_t:get() return self.cur_src:get() end
  terra cycle_t:shift()
    self.cur_src:shift()
    if not self:get().valid then
      self.cur_src = self.orig
    end
  end
  cycle_t.infinite = true

  setup_iterator(cycle_t)

  return `cycle_t{it, it}
end

function iterate.get_until(it, cond)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)
  local Cond = common.expr_type(cond)

  local struct get_until_t{ src: ItT, cond: Cond }

  terra get_until_t:get(): optional.of(T)
    if self.cond() then return {} end
    return self.src:get()
  end
  terra get_until_t:shift() self.src:shift() end

  setup_iterator(get_until_t)

  return `get_until_t{it, cond}
end

function iterate.transpose(it)
  local ItT = it:gettype()
  assert(
    iterate.is_iterator(ItT),
    string.format('expected iterator, got %s', ItT))
  assert(
    ItT.methods.size, 'iterator must be sized')
  assert(
    ItT.methods.after, 'iterator must be random-access')

  local it_of = iterate.value_type(ItT)
  assert(
    iterate.is_iterator(it_of),
    string.format('expected iterator of iterators, got iterator of %s', it_of))
  assert(
    it_of.methods.size, 'internal iterator must be sized')
  assert(
    it_of.methods.after, 'internal iterator must be random-access')

  local terra transpose_f(it_: ItT)
    if not it_:map([lam(l) l:size()]):all_equal() then
      c_io.fprintf(
        c_io.stderr,
        ['fatal error: attempted to transpose iterator of iterators where not all '..
         'contained iterators have the same size'])
    end

    -- if iterator has size 0, the transposed iterator will also have size 0
    var trans_size = it_:at(0):map([lam(l) l:size()]):value_or(0)
    return iterate.count()
      :get_n(trans_size)
      :map([lam(i: int) cap(it_) -- FIXME generic lambda fails here (explicit int decl. needed)
        it_:map([lam(l) cap(i) l:at(i):value()])])
  end

  return `transpose_f(it)

end

return iterate
