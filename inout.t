local inout = {}
local c_io = terralib.includec 'stdio.h'

local function printfmt(T, fmt)
  return terra(val: &T) return c_io.printf([fmt], @val) end
end

inout.howto_output = {
  [int8] = printfmt(int8, '%hhd'),
  [int16] = printfmt(int16, '%hd'),
  [int32] = printfmt(int32, '%d'),
  [int64] = printfmt(int64, '%ld'),
  [uint8] = printfmt(uint8, '%hhu'),
  [uint16] = printfmt(uint16, '%hu'),
  [uint32] = printfmt(uint32, '%u'),
  [uint64] = printfmt(uint64, '%lu'),
  [float] = printfmt(float, '%f'),
  [double] = printfmt(double, '%lf'),
  [&int8] = printfmt(&int8, '%s')
}

inout.outval = macro(function(val)
  local T = val:gettype()
  local outfn = assert(
    inout.howto_output[T],
    'don\'t know how to output value of type ' .. tostring(T) ..
    '\nplease provide an implementation through inout.howto_output:\n' ..
    '    inout.howto_output[' .. tostring(T) .. '] = <terra function>')

  if val:islvalue() then return `outfn(&val) end

  -- if not lvalue, store in temporary variable just so we can support taking
  -- pointers in howto_output
  return quote
    var v = [val]
  in
    outfn(&v)
  end
end)

inout.out = macro(function(val, ...)
  local rest = {...}
  if #rest == 0 then return `inout.outval(val) end

  return quote
    inout.outval(val)
    inout.out([rest])
  end
end)

return inout
