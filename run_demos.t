local demos = {
  'iterate_array_of_arrays',
  'iterate_arrays',
  'iterate_cubes',
  'iterate_cur_shift',
  'iterate_lambda',
  'iterate_size',
  'iterate_transpose',
  'lambda',
  'optional_cast',
  'optional_compare',
  'optional_map',
  'optional_of_optional',
  'result',
  'variant',
  'variant_cast',
}

for _,d in ipairs(demos) do
  print('=> running demo: ' .. d .. '\n')
  require('demos.' .. d)
  main()
  print('')
end

print '=> running demo: common_function_result'
require 'demos.common_function_result'

return 0
