-- variant.t: sum type (i.e. tagged union)
local common = require 'common'
local optional = require 'optional'
local List = require 'terralist'

local variant = {}

local function to_comma_sep_str(list, to_str)
  local function concat(str, el)
    return str .. ', ' .. tostring(el)
  end

  return string.sub(list:fold('', concat), 3)
end

function variant.of(...)
  local struct variant_t { index: uint8 }

  variant_t.types = List{...}
  variant_t.entries:insert(variant_t.types:map(function(t) return {t.name, t} end))

  variant_t.name = string.format(
    'variant.of(%s)',
    to_comma_sep_str(variant_t.types:map(function(t) return t.name end)))

  function variant_t:type_to_index(t)
    local idx = self.types
      :mapi(function(i, vt) return {i, vt} end)
      :find(function(vt) return vt[2] == t end)
    return idx and idx[1] - 1 -- zero-based index
  end

  variant_t.metamethods.__methodmissing = macro(function(method, obj, ...)
    local m = variant[method]
    assert(m, 'error: no method such as ' .. method .. ' exists for variants')

    local args = {...}
    if method == 'contained_t' or method == 'get' then
      return m(obj, args[1].tree.value)
    end

    return m(obj, ...)
  end)

  function variant_t.metamethods.__cast(from, to, exp)
    -- cast expression of particular type to variant type
    if to.type_to_index and to:type_to_index(from) then
      return quote
        var v: to
        v:set(exp)
      in
        v
      end
    end
  end

  return variant_t
end

function variant.contained_t(v, t)
  local variant_t = common.expr_type(v)
  local t_idx = variant_t:type_to_index(t)
  if not t_idx then return `false end

  local terra contained_t_f(v_: &variant_t)
    return v_.index == t_idx
  end

  return `contained_t_f(&v)
end

function variant.get(v, t)
  local variant_t = common.expr_type(v)
  local t_idx = assert(
    variant_t:type_to_index(t),
    string.format('%s doesn\'t contain type %s', variant_t, t))

  local terra get_f(v_: &variant_t): optional.of(&t)
    if v_.index ~= t_idx then return {} end

    -- must use optional.new to avoid clash between optional and variant casts
    -- FIXME prevent these clashes. they're a major source of bugs
    return optional.new(&v_.[t.name])
  end

  return `get_f(&v)
end

function variant.set(v, val)
  local variant_t = common.expr_type(v)
  local val_t = common.expr_type(val)
  local t_idx = assert(
    variant_t:type_to_index(val_t),
    string.format('can\'t set %s of type %s to %s', val, val_t, variant_t))

  local terra set_f(v_: &variant_t, val: val_t)
    v_.index = t_idx
    v_.[val_t.name] = val
  end

  return `set_f(&v, val)
end

function variant.match(v, ...)
  local variant_t = common.expr_type(v)
  local branches = List{...}
  local br_params = branches:map(function(b)
    local T = common.expr_type(b)
    return common.paramlist(T)
  end)

  -- TODO better error messages (e.g. actually single out which function is wrong)
  assert(
    br_params:all(function(params) return params and #params == 1 end),
    'variant.match expects all branches to be unary functions')
  assert(
    br_params:all(function(params) return params[1]:ispointer() end),
    'variant.match expects all branches to take pointers')

  local retvals = branches:map(function(b)
    local T = common.expr_type(b)
    return common.function_result(T)
  end)

  assert(
    retvals:all(function(ret) return ret == retvals[1] end),
    'variant.match expects all branches to have the same return type')

  local retval = retvals[1]

  -- array containing match functions
  -- TODO support generic lambdas
  local match_list = variant_t.types:map(function(var_t)
    local cur_branch = branches:find(function(br)
      local branch_t = common.paramlist(common.expr_type(br))[1].type
      return branch_t == var_t
    end)

    assert(cur_branch, string.format('variant.match: no matching branch for type %s', var_t))

    return terra(v_: &variant_t): retval
      return [cur_branch](&v_.[var_t.name])
    end
  end)
  local match_table = constant(`array([match_list]))

  local terra match_f(v_: &variant_t): retval
    return [match_table][v_.index](v_)
  end

  return `match_f(&v)
end

variant.of = terralib.memoize(variant.of)

return variant
