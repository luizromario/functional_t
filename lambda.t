local common = require 'common'

local lambda_expr = {
  name = 'lambda_expr',
  entrypoints = {'lam'},
  keywords = {'cap', 'do', 'end'}
}

local function paramdecl(lex)
  local pname = lex:expect(lex.name).value
  if not lex:nextif ':' then return {name = pname} end

  local ptype = lex:luaexpr()
  return {name = pname, typefn = ptype}
end

local function get_paramlist(lex)
  lex:expect '('

  -- empty paramlist is typed bc nullary functions don't need to be macros and can be concrete
  -- terra functions instead
  if lex:nextif ')' then return {typed = true} end

  local paramlist = {paramdecl(lex)}
  paramlist.typed = paramlist[1].typefn ~= nil

  while not lex:nextif ')' do
    lex:expect ','

    local curparam = paramdecl(lex)
    local curistyped = curparam.typefn ~= nil

    if paramlist.typed ~= curistyped then -- param list has to be either fully typed or fully untyped
      local pname = curistyped and paramlist[1].name or curparam.name
      error(string.format('missing type for parameter "%s"', pname))
    end

    table.insert(paramlist, curparam)
  end

  return paramlist
end

local function get_caplist(lex)
  if not lex:nextif 'cap' then return {} end
  lex:expect '('
  if lex:nextif ')' then return {} end

  local caplist = {lex:expect(lex.name).value}
  while not lex:nextif ')' do
    lex:expect ','
    table.insert(caplist, lex:expect(lex.name).value)
  end

  return caplist
end

local function mkparamsyms(env, paramlist)
  local symbols = {}
  for _, var_ in ipairs(paramlist) do
    local sym = symbol(var_.typefn(env))
    sym.displayname = var_.name
    table.insert(symbols, sym)
  end

  return symbols
end

local function mkcapsyms(env, caplist)
  local symbols = {}
  for _, cap in ipairs(caplist) do
    local sym = env[cap]
    table.insert(symbols, sym)
  end

  return symbols
end

local function mkclosure(env, capsyms)
  local closure_t = struct{}
  for _,s in ipairs(capsyms) do
    closure_t.entries:insert({s.displayname, s.type})
  end

  return closure_t
end

local function addtoenv(env, syms)
  for _,s in ipairs(syms) do env[s.displayname] = s end
end

local function setupclosure(env, caplist)
  local caps = mkcapsyms(env, caplist)
  local closure_t = mkclosure(env, caps)

  addtoenv(env, caps)

  local selfsym = symbol(&closure_t)
  local selfcaps = {}
  for _,cap in ipairs(caplist) do table.insert(selfcaps, `[selfsym].[cap]) end

  return {t = closure_t, caps = caps, selfsym = selfsym, selfcaps = selfcaps}
end

local function mkapply(env, closure, params, body)
  local allsyms = {}
  for _,v in ipairs(closure.caps) do table.insert(allsyms, v) end
  for _,v in ipairs(params) do table.insert(allsyms, v) end

  local lam_f
  if body.abbrev then
    lam_f = terra([allsyms]) return [body.abbrev(env)] end
  elseif body.full then
    lam_f = terra([allsyms]) [body.full(env)] end
  end

  return terra([closure.selfsym], [params])
    return [lam_f]([closure.selfcaps], [params])
  end
end

local function mktypedlambda(env, paramlist, caplist, body)
  local params = mkparamsyms(env, paramlist)
  local closure = setupclosure(env, caplist)

  addtoenv(env, params)
  closure.t.metamethods.__apply = mkapply(env, closure, params, body)

  return `[closure.t]{[closure.caps]}
end

local function mkgenericlambda(env, paramlist, caplist, body)
  local closure = setupclosure(env, caplist)

  closure.t.metamethods.__apply = macro(function(self, ...)
    -- match arg types with params
    local args = {...}
    assert(
      #args == #paramlist,
      string.format('wrong number of arguments: expected %d, but %d given', #paramlist, #args))

    local params = {}
    for i = 1, #paramlist do
      params[i] = symbol(common.expr_type(args[i]))
      params[i].displayname = paramlist[i].name
    end

    addtoenv(env, params)

    local apply = mkapply(env, closure, params, body)
    return `apply(&self, [args])
  end)

  return `[closure.t]{[closure.caps]}
end

-- simple lambda expression:
-- lam(<paramlist>) [cap(<idlist>)] <expr>
function lambda_expr:expression(lex)
  lex:expect 'lam'
  local paramlist = get_paramlist(lex)
  local caplist = get_caplist(lex)

  local body = {}
  if not lex:nextif 'do' then body.abbrev = lex:terraexpr()
  else
    body.full = lex:terrastats()
    lex:expect 'end'
  end

  return function(envfn)
    local env = envfn()
    if paramlist.typed then return mktypedlambda(env, paramlist, caplist, body)
    else return mkgenericlambda(env, paramlist, caplist, body) end
  end
end

return lambda_expr
