-- optional.t: optional (i.e. maybe-empty) values

local c_io = terralib.includec 'stdio.h'
local c_lib = terralib.includec 'stdlib.h'
local common = require 'common'

local optional = {}

-- optional.of(T): optional type, either T or empty
optional.of = terralib.memoize(function(T)
  local optional_t = struct {
    valid: bool,
    raw_value: T
  }

  optional_t.is_optional = true
  optional_t.of = T
  optional_t.name = string.format('optional.of(%s)', T)

  -- try to get contained value. exits with failure if empty
  terra optional_t:valueptr()
    if not self.valid then
      c_io.fprintf(
        c_io.stderr,
        ['fatal error: attempted to access empty ' .. tostring(optional_t) .. '\n'])
      c_lib.exit(1)
    end

    return &self.raw_value
  end

  terra optional_t:value() return @self:valueptr() end

  -- get contained value if present; otherwise, get `default`
  terra optional_t:value_or(default: T)
    if self.valid then return self.raw_value else return default end
  end

  function optional_t.metamethods.__cast(from, to, exp)
    -- cast {} to empty optional
    if from == tuple() and to.is_optional then
      -- forces execution of exp (might be a function returning {})
      return quote exp in to{false} end
    end

    -- cast value to optional
    if from == to.of then return `to{true, exp} end
  end

  optional_t.metamethods.__methodmissing = macro(function(method, obj, ...)
    local m = optional[method]
    assert(m, 'error: no method such as ' .. method .. ' exists for optionals')

    return m(obj, ...)
  end)

  -- comparison
  optional_t.metamethods.__eq = macro(function(o1, o2)
    local Opt = common.expr_type(o1)

    local both_null = `not o1.valid and not o2.valid
    local both_valid = `o1.valid and o2.valid
    local vals_equal = `o1:value() == o2:value()

    return `[both_null] or ([both_valid] and [vals_equal])
  end)

  terra optional_t.metamethods.__ne(lhs: &optional_t, rhs: &optional_t)
    return not (lhs == rhs)
  end

  return optional_t
end)

-- create instance of optional from value
optional.new = macro(function(val)
  return `[optional.of(val:gettype())]{true, val}
end)

-- create empty optional of given type. if `T` is already optional, don't further nest it
function optional.nullof(T)
  local OptT
  if T.is_optional then OptT = T else OptT = optional.of(T) end

  return `OptT{false}
end

-- operate on the internal optional value (if present) without unwrapping.
function optional.map(opt, f)
  local Opt = common.expr_type(opt)
  local T = Opt.of
  if common.is_lvalue(opt) then Opt = &Opt end

  local F = common.expr_type(f)
  local terra map_f(opt: Opt, f: F): optional.of(common.function_result(F, T))
    if not opt.valid then return {} end

    -- must use optional.new to avoid clash between optional and variant/result casts
    -- FIXME prevent these clashes. they're a major source of bugs
    return optional.new(f(opt.raw_value))
  end

  if common.is_lvalue(opt) then  return `map_f(&opt, f)
  else return `map_f(opt, f) end
end

return optional
