local inout = require 'inout'
local it = require 'iterate'
import 'lambda'

terra main()
  var c = it.count()
  inout.out('c initial value: ', c:get():value(), '\n')

  c:after(10)
  inout.out('c ten positions later: ', c:get():value(), '\n')

  var sqrs = it.count():map([lam(n) n*n])
  sqrs:after(1000)

  inout.out('thousandth square number: ', sqrs:get():value(), '\n')

  -- filter will lose the after method
  var sqrs_filtered = sqrs:filter([lam(n) n > 123456789]):map([lam(n) n/2])
  inout.out('that thing I did: ', sqrs_filtered:get():value(), '\n')

  -- sqrs_filtered:after(10) -- no such method after for iterators
  sqrs_filtered:shift() -- OK
  inout.out('that thing I did (again): ', sqrs_filtered:get():value(), '\n')

  var arr = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
  var arrit = it.from_array(arr)
  arrit:after(10)

  inout.out('array after 10 positions: ', @arrit:get():value(), '\n')

  arrit:after(-10)
  inout.out('should be first element: ', @arrit:get():value(), '\n')

  var arrsqrs = arrit:map([lam(n) @n * @n])
  arrsqrs:after(10)
  inout.out('tenth array element squared: ', arrsqrs:get():value(), '\n')

  arrsqrs:after(-10)

  var arrsqrs_gt123 = arrsqrs:filter([lam(n) n > 123])
  inout.out('first array element squared greater than 123: ', arrsqrs_gt123():value(), '\n')

  return 0
end

terralib.saveobj('iterate_cur_shift', {main = main})
