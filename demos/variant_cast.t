local inout = require 'inout'
local variant = require 'variant'

local number_or_string = variant.of(int, float, rawstring)

terra main()
  var n1: number_or_string = 10
  var n2: number_or_string = 10.f
  var n3: number_or_string = 'hello'

  inout.out('n1: ', @n1:get(int):value(), '\n')
  inout.out('n2: ', @n2:get(float):value(), '\n')
  inout.out('n3: ', @n3:get(rawstring):value(), '\n')

  return 0
end

terralib.saveobj('variant_cast', {main = main})
