local inout = require 'inout'
local variant = require 'variant'
import 'lambda'

local var_int_float = variant.of(int, float, variant.of(int32, &int8))

terra vartest(v: &var_int_float)
  inout.out '======\n'
  if v:contained_t(int) then
    inout.out('type is int\n')
  elseif v:contained_t(float) then
    inout.out('type is float\n')
  elseif v:contained_t([variant.of(int32, &int8)]) then
    inout.out('type is another variant\n')
  end

  var vint = v:get(int)
  vint:map([lam(n) inout.out('vint: ', @n, '\n')])

  var vfloat = v:get(float)
  vfloat:map([lam(n) inout.out('vfloat: ', @n, '\n')])

  v:get([variant.of(int32, &int8)])
    :map([lam(ov) do
      ov:get(int):map([lam(n) inout.out('other variant int: ', @n, '\n')])
      ov:get([&int8]):map([lam(str) inout.out('other variant str: ', @str, '\n')])
    end])

  var vplus = v:match(
    [lam(n: &int) [float](@n + 1)],
    [lam(n: &float) @n + 1],
    [lam(n: &variant.of(int, &int8))
      n:match(
        [lam(nn: &int) [float](@nn + 2)],
        [lam(nn: &&int8) 0.0f])])

  inout.out('vplus: ', vplus, '\n')
end

terra main()
  var v: var_int_float
  v:set(10)
  vartest(&v)

  v:set(10.0f)
  vartest(&v)

  v:set([variant.of(int32, &int8)]{})
  v:get([variant.of(int32, &int8)]):map([lam(ov) ov:set(20)])
  vartest(&v)

  v:get([variant.of(int32, &int8)]):map([lam(ov) ov:set('hello')])
  vartest(&v)

  return 0
end

terralib.saveobj('variant', {main = main})
