import 'lambda'
local inout = require 'inout'
local iterate = require 'iterate'

terra main()
  var data: int[3][3]
  for i = 0, 3 do
    for j = 0, 3 do
      data[i][j] = i*3 + j
    end
  end

  for i = 0, 3 do
    for j = 0, 3 do
      inout.out(data[i][j], '\t')
    end
    inout.out '\n'
  end
  inout.out '\n'

  iterate.from_array(data)
    :map([lam(r) iterate.from_array(@r)])
    :each_of([lam(r) do
      r:each_of([lam(n) inout.out(@n, '\t')])
      inout.out '\n'
    end])

  return 0
end

terralib.saveobj('iterate_array_of_arrays', {main = main})
