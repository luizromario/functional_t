import 'lambda'

local c_io = terralib.includec 'stdio.h'
local optional = require 'optional'

local ooint = optional.of(optional.of(int))

terra main()
  var n: ooint = optional.new(10)
  n:map([lam(n) n:map([lam(n)
    c_io.printf('n: optional(optional(%d))\n', n)])])

  var valid_opt = n:map([lam(n) n.valid])
  if valid_opt:value() then c_io.printf 'optional(true)\n' end

  n = {}
  if not n.valid then c_io.printf 'n invalid (as expected)\n' end

  n = [optional.nullof(int)]
  if not n:value().valid then c_io.printf 'optional(nullopt)\n' end

  return 0
end

terralib.saveobj('optional_of_optional', {main = main})
