import 'lambda'

local iterate = require 'iterate'
local c_io = terralib.includec 'stdio.h'

terra nbigcubes(n: int)
  return iterate.count()
    :map([lam(x) x*x*x]):filter([lam(x) x > 12345]):get_n(n)
end

terra main()
  var c1 = iterate.count()
  var c2 = iterate.count()

  c1() c1() c1()
  c2() c2()

  c_io.printf("c1: %d\n", c1():value())
  c_io.printf("c2: %d\n", c2():value())

  var ns = iterate.count():get_n(10)
  ns() ns() ns() ns() ns() ns() ns() ns() ns() ns()
  if ns().valid then c_io.printf("ns still valid\n") else c_io.printf("ns no longer valid\n") end

  [iterate.each_of(iterate.get_n(`iterate.count(), 10),
                   lam(n) c_io.printf("%d ", n))]
  c_io.printf('\n')

  [iterate.each_of(`iterate.from_array(array(1, 2, 3, 4, 5, 6)),
                   lam(n) c_io.printf("%d ", @n))]
  c_io.printf('\n')

  var sqrs = [iterate.get_n(iterate.map(`iterate.count(), lam(n) n*n), 10)]
  sqrs:each_of([lam(n) c_io.printf("%d ", n)])
  c_io.printf("\n")

  c_io.printf("These are the first 10 cubes larger than 12345:\n")

  var ten_big_cubes = nbigcubes(10)

  [iterate.each_of(ten_big_cubes, lam(n) c_io.printf("%d ", n))]
  c_io.printf("\n")

  return 0
end

terralib.saveobj("iterate_cubes", {main = main})
