local c_io = terralib.includec 'stdio.h'
local optional = require 'optional'

terra main()
  var n1 = optional.new(10)
  var n2: optional.of(int) = {}

  if n1 ~= n2 then c_io.printf('%d and null are different\n', n1:value()) end

  var nn1 = optional.new(n1)
  var nn2 = optional.new(n2)
  var nnempty: optional.of(optional.of(int)) = {}

  if nn1 ~= nn2 then c_io.printf('%d and null are different\n', nn1:value():value()) end
  if nn1 == nn1 then c_io.printf('%d is equal to itself\n', nn1:value():value()) end

  var n3 = optional.new(array(10, 20, 30))
  -- if n3 == n3 then c_io.printf('n3 equal to itself\n') end -- won't compile

  return 0
end

terralib.saveobj('optional_compare', {main = main})
