local opt = require 'optional'
local c_io = terralib.includec 'stdio.h'

terra main()
  var nul: opt.of(int) = {}
  c_io.printf('%d\n', nul:value_or(-1))

  var not_nul: opt.of(int) = 10
  c_io.printf('%d\n', not_nul:value())

  var dez = opt.new(10)
  c_io.printf('%d\n', dez:value())

  nul = dez
  c_io.printf('%d\n', nul:value())

  nul = {}
end

terralib.saveobj('optional_cast', {main = main})
