-- tic-tac-toe game
import 'lambda'
local c_io = terralib.includec 'stdio.h' -- FIXME just for scanf
local inout = require 'inout'
local it = require 'iterate'
local opt = require 'optional'
local result = require 'result'
local variant = require 'variant'

local player = &int8
local maxsize = 16
local maxplayers = 6
local line = opt.of(player)[maxsize]

struct game {
  players_data: player[maxplayers]
  players_size: int
  lines_data: line[maxsize]
  size: int
}

terra game:init(size: int, players_size: int)
  self.size = size
  self.players_size = players_size

  -- TODO let users determine player character
  self.players_data = array('X', 'O', '@', '#', '$', '*')

  self:lines():each_of([lam(l)
    l:each_of([lam(p) do @p = {} end])])
end

terra game:lines()
  return it.from_array(self.lines_data):get_n(self.size)
    :map([lam(l) cap(self) it.from_array(@l):get_n(self.size)])
end

terra game:players()
  return it.from_array(self.players_data):get_n(self.players_size)
end

terra game:print()
  self:lines():each_of([lam(l) do
    inout.out '|'
    l:each_of([lam(p) inout.out(p:value_or(' '), ' ')])
    inout.out '|\n'
  end])
  inout.out '\n'
end

struct position_taken{ r: int, c: int }
struct invalid_column{ c: int }
struct invalid_row{ r: int }
local play_error = variant.of(position_taken, invalid_column, invalid_row)
local play_result_t = result.of(tuple(), play_error)

terra game:play_at(p: player, r: int, c: int): play_result_t
  var i, j = r - 1, c - 1
  return self:lines():at(i):map([lam(l) cap(j, p, r, c)
    l:at(j):map([lam(atpos) cap(p, r, c) do
      if atpos.valid then
        return [play_result_t.err](position_taken{r, c})
      end

      @atpos = p
      return [play_result_t.ok]({})
    end])
    :value_or([play_result_t.err](invalid_column{c}))])
  :value_or([play_result_t.err](invalid_row{r}))
end

terra game:draw(): bool
  return self:lines():all_of([lam(l) l:all_of([lam(p) p.valid])])
end

local winning_line = macro(function(it)
  return `it:all_equal() and it:all_of([lam(p) p.valid])
end)

terra game:win_row(): bool
  return self:lines():any_of([lam(l) winning_line(l:map([lam(p) @p]))])
end

terra game:win_col(): bool
  return self:lines():transpose():any_of([lam(l) winning_line(l:map([lam(p) @p]))])
end

terra game:win_diag(): bool
  var size = self:lines():size()
  var diag = it.count():get_n(size)
    :map([lam(i) cap(self) @self:lines():at(i):value():at(i):value()])
  var anti_diag = it.count():get_n(size)
    :map([lam(i) cap(self, size) @self:lines():at(i):value():at(size - i - 1):value()])

  return winning_line(diag) or winning_line(anti_diag)
end

terra game:win(): bool
  return self:win_row() or self:win_col() or self:win_diag()
end

terra game:over()
  return self:win() or self:draw()
end

terra game:player_turn(p: player)
  self:print()
  repeat
    var row: int, column: int
    inout.out(p, '\'s turn. Choose a position (row, column): ')
    c_io.scanf('%d, %d', &row, &column) -- FIXME no error handling, scanf errors can be pretty chaotic

    -- TODO `get_error' that returns optional of the error type
    var play_result = self:play_at(p, row, column)
    if not play_result:valid() then
      var err = play_result:error()
      err:match(
        [lam(err: &position_taken) inout.out('position taken: ', err.r, ', ', err.c)],
        [lam(err: &invalid_column) inout.out('invalid column: ', err.c)],
        [lam(err: &invalid_row) inout.out('invalid row: ', err.r)])
      inout.out '\n'
    end
  until play_result:valid()
end

terra game:loop()
  self:players()
    :cycle()
    :get_until([lam() cap(self) self:over()])
    :each_of([lam(p) cap(self) self:player_turn(@p)])
end

terra main()
  var gamesize: int
  inout.out 'Choose the game size: '
  c_io.scanf('%d', &gamesize)

  if gamesize < 3 then
    inout.out 'Too small! The game should be at least 3x3\n'
    return 1
  elseif gamesize > maxsize then
    inout.out('Too big! The game should be no larger than ', maxsize, 'x', maxsize, '\n')
    return 1
  end

  var players_size: int
  inout.out 'How many players? '
  c_io.scanf('%d', &players_size)

  if players_size < 2 then
    inout.out 'This is not a single-player game. You need at least two players\n'
    return 1
  elseif players_size > maxplayers then
    inout.out('This game holds at most ', maxplayers, ' players\n')
    return 1
  end

  inout.out('Starting ', gamesize, 'x', gamesize, ' game\n')

  var g: game
  g:init(gamesize, players_size)
  g:loop()

  g:print()
  if g:win() then inout.out 'Victory!\n'
  elseif g:draw() then inout.out 'It\'s a draw\n' end

  return 0
end

terralib.saveobj('velha', {main = main})
