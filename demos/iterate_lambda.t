import 'lambda'

local iterate = require 'iterate'
local util = require 'util'
local c_io = terralib.includec 'stdio.h'

terra intexp(x: int, n: int): int
  if n <= 0 then return 1 end

  return x * intexp(x, n - 1)
end

terra bigexp(n: int, gt: int, e: int)
  return iterate.count()
    :map([lam(x) cap(e) intexp(x, e)])
    :filter([lam(x) cap(gt) x > gt])
    :get_n(n)
end

terra main()
  c_io.printf('fifteen first cubes larger than 20 thousand:\n')
  var cubes = bigexp(15, 20000, 3)
  [util.printfline(cubes, '%d')]

  c_io.printf('\nfive first powers of five larger than one billion:\n')
  var fifth_one_billion = bigexp(5, 999999999, 5)
  [util.printfline(fifth_one_billion, '%d')]

  return 0
end

terralib.saveobj('iterate_fobj', {main = main})
