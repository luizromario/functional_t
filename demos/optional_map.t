import 'lambda'

local opt = require 'optional'
local c_io = terralib.includec 'stdio.h'

struct activity
{
  name: &int8,
  started: int,
  finished: opt.of(int)
}

terra activity:duration()
  return self.finished:map([lam(finished: int) cap(self)
    finished - self.started
  ])
end

terra activity:print()
  c_io.printf('activity %s started at time %d', self.name, self.started)
  self:duration():map([lam(duration: int) cap(self)
    self.finished:map([lam(finished: int) cap(duration)
      c_io.printf(' and finished at time %d (duration %d)', finished, duration)
    ])
  ])

  c_io.printf('\n')
end

terra main()
  var ten: opt.of(int) = 10
  var twenty = ten:map([lam(n: int) n + 10])
  c_io.printf('twenty = %d\n', twenty:value())

  var empty: opt.of(int) = {}
  var emptyplus10 = empty:map([lam(n: int) n + 10])
  if not emptyplus10.valid then c_io.printf('empty plus 10 is still empty\n') end

  var ten_inverse = ten:map([lam(n) 1.0 / n])
  c_io.printf('1/10 = %f\n', ten_inverse:value())

  var dishes = activity{'dishes', 100, {}}
  dishes:print()

  dishes.finished = 200
  dishes:print()

  return 0
end

terralib.saveobj('optional_map', {main = main})
