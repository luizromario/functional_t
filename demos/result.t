local inout = require 'inout'
local result = require 'result'
local optional = require 'optional'
import 'lambda'

terra divopt(n: float, m: float): optional.of(float)
  if m == 0.f then return {} end

  return n/m
end

local div_result = result.of(float, rawstring)
terra divres(n: float, m: float)
  return divopt(n, m)
    :map([lam(d) [div_result.ok](d)])
    :value_or([div_result.err]('cannot divide by zero')) -- FIXME remove splicing
end

terra normal_div(n: float, m: float): result.of(float, rawstring)
  if m == 0.f then return result.error('cannot divide by zero') end

  return n/m
end

terra test_div(div: {float, float} -> div_result)
  var onehalf = div(1,2)
  inout.out('onehalf: ', @onehalf:value(), '\n')

  if onehalf:valid() then
    inout.out 'onehalf valid\n'
  end

  var divbyzero = div(1,0)
  inout.out('divbyzero: ', @divbyzero:error(), '\n')

  if not divbyzero:valid() then
    inout.out 'divbyzero invalid\n'
  end
end

terra main()
  test_div(normal_div)
  test_div(divres)

  return 0
end

terralib.saveobj('result', {main = main})
