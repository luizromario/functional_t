import 'lambda'

local iterate = require 'iterate'
local util = require 'util'
local c_io = terralib.includec 'stdio.h'
local c_math = terralib.includec 'math.h'

terra main()
  var arr = array(1, 2, 3, 4, 5, 6)
  var arrlen = [arr.type.N]

  var arr_it = iterate.from_array(arr)
  [util.printfline(`arr_it:map([lam(n) @n]), '%d')]

  var arrptr: &int = arr
  var arrptr_it = iterate.from_array_ptr(arrptr, arrlen)
  [util.printfline(`arrptr_it:map([lam(n) @n]), '%d')]

  [util.printfline(`iterate.from_array(arr):map([lam(n) @n]), '%d')]
  [util.printfline(`iterate.from_array(array(1, 2, 3, 4, 5, 6)):map([lam(n) @n]), '%d')]

  [util.printfline(
     iterate.map(`iterate.from_array(arr), lam(n) c_math.sqrt(@n)), '%f')]

  return 0
end

terralib.saveobj('iterate_arrays', {main = main}, {'-l', 'm'})
