local inout = require 'inout'
local it = require 'iterate'
import 'lambda'

terra main()
  var arr = array(1, 2, 3, 4, 5, 6)
  var arrit = it.from_array(arr)
  inout.out('arrit size: ', arrit:size(), '\n')

  var c = it.count()
  -- c:size() -- iterator count_t does not have a size

  var inverses = arrit:map([lam(n) 1.0 / @n])
  inout.out('inverses size: ', inverses:size(), '\n')

  var sqrs = c:map([lam(n) n * n])
  -- iterator map_t$1 does not have a size: source iterator count_t does not have a size
  -- sqrs:size()

  var front = arrit:get_n(3)
  inout.out('front size: ', front:size(), '\n')

  var more_than_source = arrit:get_n(8)
  inout.out('more_than_source size: ', more_than_source:size(), '\n')

  var cyc = arrit:cycle():get_n(123)
  inout.out('cyc size: ', cyc:size())

  return 0
end

terralib.saveobj('iterate_size', {main = main})
