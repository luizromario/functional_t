import 'lambda'
local common = require 'common'

local f1 = terra() return 10 end
local F1 = common.expr_type(f1)
assert(common.function_result(F1) == int)

local f2 = lam(x: float) x
local F2 = common.expr_type(f2)
assert(common.function_result(F2, float) == float)

local f3 = lam(x) x
local F3 = common.expr_type(f3)
assert(common.function_result(F3, int8) == int8)
assert(common.function_result(F3, float) == float)
