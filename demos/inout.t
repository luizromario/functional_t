local inout = require 'inout'
local c_io = terralib.includec 'stdio.h'

struct Point
{
  x: int,
  y: int
}

inout.howto_output[Point] = terra(p: &Point)
  c_io.printf('(%d, %d)', p.x, p.y)
end

terra main()
  inout.outval(10)
  inout.outval(' ')
  inout.outval(10.0)
  inout.outval('\n')

  inout.out('Olá: ', 10, '\n')

  var p = Point{10, 20}
  inout.out('Ponto: ', p, '\n')

  return 0
end

terralib.saveobj('inout', {main = main})
