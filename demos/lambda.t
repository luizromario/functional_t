import 'lambda'

local common = require 'common'
local c_io = terralib.includec 'stdio.h'

terra main()
  var n = 10
  var m = 20
  var f = [lam(x: int, y: int) cap(n, m) x + y + n + m]
  c_io.printf('%d\n', f(11, 239))

  var f2 = [lam(x: int) cap(f, n) do
              var sqr = x * x
              return f(sqr, n)
           end]
  c_io.printf('%d\n', f2(50))

  var f3 = [lam(x) x*x*x]
  c_io.printf('%d\n', f3(10))
  c_io.printf('%f\n', f3(10.1))

  var f4 = [lam(x) cap(f3) f3(x) + x]
  c_io.printf('%d\n', f4(10))
  c_io.printf('%f\n', f4(10.1))

  var f5 = [lam(x, y) x + y]
  -- f5() -- wrong number of arguments, expected 2, but 0 given
  c_io.printf('%f\n', f5(10, 20.1))

  return 0
end

terralib.saveobj('lambda', {main = main})
