local inout = require 'inout'
local iterate = require 'iterate'
import 'lambda'

terra main()
  var arrayofarrays = array(
    array(1, 2, 3, 4),
    array(5, 6, 7, 8),
    array(9, 10, 11, 12)
  )

  var arrofarrsit = iterate.from_array(arrayofarrays)
    :map([lam(arr) iterate.from_array(@arr)])

  var transposed = arrofarrsit:transpose()
  inout.out('transposed size: ', transposed:size(), '\n')

  inout.out '\noriginal matrix\n'
  arrofarrsit:each_of([lam(l) do
    l:each_of([lam(n) inout.out(@n, '\t')])
    inout.out '\n'
  end])

  inout.out '\ntransposed matrix\n'
  transposed:each_of([lam(l) do
    l:each_of([lam(n) inout.out(@n, '\t')])
    inout.out '\n'
  end])

  return 0
end

terralib.saveobj('iterate_transpose', {main = main})
