import 'lambda'

local common = require 'common'
local iterate = require 'iterate'
local c_io = terralib.includec 'stdio.h'

local util = {}

function util.printfline(it, fmt)
  local ItT = common.expr_type(it)
  local T = iterate.value_type(ItT)

  return quote
    [iterate.each_of(it, lam(e: T) c_io.printf([fmt .. ' '], e))]
    c_io.printf('\n')
  end
end

return util
